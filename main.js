var cubeRotation = 0.0;

var c;
var p; 
var cback ; 
f = [] ;
rail_track = [] ;  
walls = [] ;
obstacle_1 = [] ;
obstacle_1_face = [] ; 
obstacle_2 = [] ;
obstacle_3 = [] ; 
obstacle_4 = [] ; 
obstacle_4_face = [] ; 
powerup_1 = [] ; 
powerup_2 = [] ; 
powerup_3 = [] ; 
coin_list = [] ; 

hit_timer = 100 ; 
initial_timer = 200 ; 
timer_duck = 60 ; 
timer_jumpingboots = 0 ;
timer_flyingboost = 0 ; 
timer_invulnerability = 0 ; 

score = 0 ; 

grayscale = false ; 

hit_no = 2 ;

main(f);

//
// Start here
//

function main(f) {

  const canvas = document.querySelector('#glcanvas');
  const gl = canvas.getContext('webgl') || canvas.getContext('experimental-webgl');

  const groundtexture = loadTexture(gl, 'track_01(1).png');
  const treetexture = loadTexture(gl, 'grass4.jpg') ; 
  const graveltexture = loadTexture(gl, 'gravel1.jpg') ; 
  const trainfront = loadTexture(gl, 'train9.jpg') ; 
  const walltexture = loadTexture(gl, 'wall2.jpg') ; 
  const electricboxtexture = loadTexture(gl, 'electric_box1.jpg') ; 
  const personbacktexture = loadTexture(gl, 'running1.jpg') ; 
  const policebacktexture = loadTexture(gl, 'policeback1.jpg') ; 
  const dogtexture = loadTexture(gl, 'dog1.jpg') ; 

  c = new cube(gl, [0.0, 0.0, -3.0]);
  cback =  new personBack(gl, [0.0, 0.0, -3.0]) ; 
  p = new police(gl, [0.0, 0.0, 0.0]) ; 
  pback = new policeBack(gl, [0.0, 0.0, 0.0]) ; 
  doggo = new dog(gl, [0.0,0.0,0.0]) ; 
 
  // tf = new Tfloor(gl, [0.0, 0.0, -20.0]) ; 

  for(var i = 0 ; i < 1000; ++i){
    for(var j = 0 ; j < 3 ; ++j){
      temp = new Tfloor(gl, [-5.0+j*5, 0.0, -2.0-i*2.0]);
      rail_track.push(temp) ; 
    }
  }
  for(var i = 0 ; i < 1000; ++i){
    for(var j = 0 ; j < 2 ; ++j){
      temp = new Tfloor(gl, [-2.5+5*j, 0.0, -2.0-i*2.0]) ; 
      f.push(temp) ; 
    }
  }

  for(var i = 0 ; i < 100 ; ++i){
    temp = new wall(gl, [-6.5, 1.0, -6.0-i*10.0]) ; 
    walls.push(temp) ; 
    temp = new wall(gl, [6.5, 1.0, -6.0-i*10.0]) ; 
    walls.push(temp) ; 
  }

  for(var i = 0 ; i < 100 ; ++i){
    cn = new coin(gl, [5.0 - 10 * Math.random(), 0.0, -20.0 - Math.random() * i * 50]) ; 
    coin_list.push(cn) ; 
    // cn = new coin(gl, [5.0 - 10 * Math.random(), 0.0, -20.0 - Math.random() * i * 50]) ; 
    // coin_list.push(cn) ; 
  }


  obstacle_1.push(new obstacleOne(gl, [0.0, 0.0, -20.0])) ; 
  obstacle_1.push(new obstacleOne(gl, [5.0, 0.0, -40.0])) ; 
  obstacle_1.push(new obstacleOne(gl, [-5.0, 0.0, -80.0])) ; 
  obstacle_1.push(new obstacleOne(gl, [5.0, 0.0, -75.0])) ; 
  obstacle_1.push(new obstacleOne(gl, [-5.0, 0.0, -120.0])) ; 
  obstacle_1.push(new obstacleOne(gl, [0.0, 0.0, -100.0])) ; 
  obstacle_1.push(new obstacleOne(gl, [5.0, 0.0, -200.0])) ; 
  obstacle_1.push(new obstacleOne(gl, [5.0, 0.0, -2000.0])) ; 
  
  obstacle_1_face.push(new trainFace(gl, [0.0, 0.0, -20.0])) ; 
  obstacle_1_face.push(new trainFace(gl, [5.0, 0.0, -40.0])) ; 
  obstacle_1_face.push(new trainFace(gl, [-5.0, 0.0, -80.0])) ; 
  obstacle_1_face.push(new trainFace(gl, [5.0, 0.0, -75.0])) ; 
  obstacle_1_face.push(new trainFace(gl, [-5.0, 0.0, -120.0])) ; 
  obstacle_1_face.push(new trainFace(gl, [0.0, 0.0, -100.0])) ; 
  obstacle_1_face.push(new trainFace(gl, [5.0, 0.0, -200.0])) ; 
  obstacle_1_face.push(new trainFace(gl, [5.0, 0.0, -2000.0])) ; 

  obstacle_2.push(new obstacleTwo(gl, [2.5, 0.0, -20.0])) ; 
  obstacle_2.push(new obstacleTwo(gl, [-2.5, 0.0, -30.0])) ; 
  obstacle_2.push(new obstacleTwo(gl, [2.5, 0.0, -50.0])) ; 
  obstacle_2.push(new obstacleTwo(gl, [0.0, 0.0, -75.0])) ; 
  obstacle_2.push(new obstacleTwo(gl, [2.5, 0.0, -85.0])) ; 
  obstacle_2.push(new obstacleTwo(gl, [-2.5, 0.0, -100.0])) ; 
  obstacle_2.push(new obstacleTwo(gl, [-2.5, 0.0, -200.0])) ; 
  obstacle_2.push(new obstacleTwo(gl, [-2.5, 0.0, -2000.0])) ; 

  obstacle_3.push(new obstacleThree(gl, [-0.75, 0.0, -40.0])) ; 
  obstacle_3.push(new obstacleThree(gl, [-0.75+5, 0.0, -80.0])) ; 
  obstacle_3.push(new obstacleThree(gl, [-0.75-5, 0.0, -90.0])) ; 
  obstacle_3.push(new obstacleThree(gl, [-0.75, 0.0, -120.0])) ; 
  obstacle_3.push(new obstacleThree(gl, [-0.75, 0.0, -160.0])) ; 
  obstacle_3.push(new obstacleThree(gl, [-0.75-5, 0.0, -1800.0])) ; 


  obstacle_4.push(new obstacleFour(gl, [2.5, 0.0, -33.0])) ; 
  obstacle_4.push(new obstacleFour(gl, [-2.5, 0.0, -78.0])) ; 
  obstacle_4.push(new obstacleFour(gl, [2.5, 0.0, -105.0])) ; 
  obstacle_4.push(new obstacleFour(gl, [-2.5, 0.0, -120.0])) ; 
  obstacle_4.push(new obstacleFour(gl, [0.0, 0.0, -190.0])) ; 
  obstacle_4.push(new obstacleFour(gl, [2.5, 0.0, -2000.0])) ; 
  
  obstacle_4_face.push(new electricBoxFace(gl, [2.5, 0.0, -33.0])) ; 
  obstacle_4_face.push(new electricBoxFace(gl, [-2.5, 0.0, -78.0])) ; 
  obstacle_4_face.push(new electricBoxFace(gl, [2.5, 0.0, -105.0])) ; 
  obstacle_4_face.push(new electricBoxFace(gl, [-2.5, 0.0, -120.0])) ; 
  obstacle_4_face.push(new electricBoxFace(gl, [0.0, 0.0, -190.0])) ; 
  obstacle_4_face.push(new electricBoxFace(gl, [2.5, 0.0, -2000.0])) ; 

  powerup_1.push(new jumpingBoots(gl, [0.0, 2.5, -35.0])) ;
  powerup_1.push(new jumpingBoots(gl, [5.0, 0.0, -75.0])) ;
  powerup_1.push(new jumpingBoots(gl, [0.0, 2.5, -105.0])) ;
  powerup_1.push(new jumpingBoots(gl, [-5.0, 2.5, -150.0])) ;
  powerup_1.push(new jumpingBoots(gl, [-5.0, 2.5, -1500.0])) ;

  powerup_2.push(new flyingBoost(gl, [0.0,0.0, -45.0])) ;
  powerup_2.push(new flyingBoost(gl, [5.0, 0.0, -65.0])) ;
  powerup_2.push(new flyingBoost(gl, [0.0,0.0, -115.0])) ;
  powerup_2.push(new flyingBoost(gl, [-5.0,0.0, -130.0])) ;
  powerup_2.push(new flyingBoost(gl, [-5.0,0.0, -1300.0])) ;

  powerup_3.push(new invulnerability(gl, [5.0,0.0, -35.0])) ;
  powerup_3.push(new invulnerability(gl, [0.0,0.0, -85.0])) ;
  powerup_3.push(new invulnerability(gl, [0.0,0.0, -130.0])) ;
  powerup_3.push(new invulnerability(gl, [5.0,0.0, -1300.0])) ;

  // If we don't have a GL context, give up now

  if (!gl) {
    alert('Unable to initialize WebGL. Your browser or machine may not support it.');
    return;
  }

  // Vertex shader program[0.0, 0.0, -3.0]

  const vsSource = `
    attribute vec4 aVertexPosition;
    attribute vec4 aVertexColor;

    uniform mat4 uModelViewMatrix;
    uniform mat4 uProjectionMatrix;

    varying lowp vec4 vColor;

    void main(void) {
      gl_Position = uProjectionMatrix * uModelViewMatrix * aVertexPosition;
      vColor = aVertexColor;
    }
  `;

  // Fragment shader program

  const fsSource = `
    varying lowp vec4 vColor;

    void main(void) {
      gl_FragColor = vColor;
    }
  `;

  // Initialize a shader program; this is where all the lighting
  // for the vertices and so forth is established.
  const shaderProgram = initShaderProgram(gl, vsSource, fsSource);

  // Collect all the info needed to use the shader program.
  // Look up which attributes our shader program is using
  // for aVertexPosition, aVevrtexColor and also
  // look up uniform locations.
  const programInfo = {
    program: shaderProgram,
    attribLocations: {
      vertexPosition: gl.getAttribLocation(shaderProgram, 'aVertexPosition'),
      vertexColor: gl.getAttribLocation(shaderProgram, 'aVertexColor'),
    },
    uniformLocations: {
      projectionMatrix: gl.getUniformLocation(shaderProgram, 'uProjectionMatrix'),
      modelViewMatrix: gl.getUniformLocation(shaderProgram, 'uModelViewMatrix'),
    },
  };


  const vsSourceTexture = `
  attribute vec4 aVertexPosition;
  attribute vec2 aTextureCoord;

  uniform mat4 uModelViewMatrix;
  uniform mat4 uProjectionMatrix;

  varying highp vec2 vTextureCoord;

  void main(void) {
    gl_Position = uProjectionMatrix * uModelViewMatrix * aVertexPosition;
    vTextureCoord = aTextureCoord;
  }
`;
  const fsSourceTexture = `
  varying highp vec2 vTextureCoord;

  uniform sampler2D uSampler;

  void main(void) {
    gl_FragColor = texture2D(uSampler, vTextureCoord);
  }
  `;
  
  // Here's where we call the routine that builds all the
  // objects we'll be drawing.
  //const buffers
    // Initialize a shader program; this is where all the lighting
  // for the vertices and so forth is established.
  const shaderProgramTexture = initShaderProgram(gl, vsSourceTexture, fsSourceTexture);

  // Collect all the info needed to use the shader program.
  // Look up which attributes our shader program is using
  // for aVertexPosition, aVevrtexColor and also
  // look up uniform locations.
  const programInfoTexture = {
    program: shaderProgramTexture,
    attribLocations: {
      vertexPosition: gl.getAttribLocation(shaderProgramTexture, 'aVertexPosition'),
      textureCoord: gl.getAttribLocation(shaderProgramTexture, 'aTextureCoord'),
    },
    uniformLocations: {
      projectionMatrix: gl.getUniformLocation(shaderProgramTexture, 'uProjectionMatrix'),
      modelViewMatrix: gl.getUniformLocation(shaderProgramTexture, 'uModelViewMatrix'),
      uSampler: gl.getUniformLocation(shaderProgram, 'uSampler'),
    },
  };


  const gray_fsSource = `
    precision mediump float;
    varying lowp vec4 vColor;

    void main(void) {
      float gray = dot(vec3(vColor[0], vColor[1], vColor[2]), vec3(0.299, 0.587, 0.114));
      gl_FragColor = vec4(vec3(gray), 1.0);
    }
  `;

  const gray_texture_fsSource = `
    varying highp vec2 vTextureCoord;

    uniform sampler2D uSampler;
    precision mediump float;

    void main(void) {
      vec4 color = texture2D(uSampler, vTextureCoord);
      float gray = dot(vec3(color[0], color[1], color[2]), vec3(0.299, 0.587, 0.114));
      gl_FragColor = vec4(vec3(gray), 1.0);
    }
  `;

  const shaderProgramGrayScale = initShaderProgram(gl, vsSourceTexture, gray_texture_fsSource);

  const programInfoTextureGreyScale = {
    program: shaderProgramGrayScale,
    attribLocations: {
      vertexPosition: gl.getAttribLocation(shaderProgramGrayScale, 'aVertexPosition'),
      textureCoord: gl.getAttribLocation(shaderProgramGrayScale, 'aTextureCoord'),
    },
    uniformLocations: {
      projectionMatrix: gl.getUniformLocation(shaderProgramGrayScale, 'uProjectionMatrix'),
      modelViewMatrix: gl.getUniformLocation(shaderProgramGrayScale, 'uModelViewMatrix'),
      uSampler: gl.getUniformLocation(shaderProgram, 'uSampler'),
    },
  };

  const shaderProgramTextureGrayScale = initShaderProgram(gl, vsSource, gray_fsSource);

  const programInfoGrey = {
    program: shaderProgramTextureGrayScale,
    attribLocations: {
      vertexPosition: gl.getAttribLocation(shaderProgramTextureGrayScale, 'aVertexPosition'),
      vertexColor: gl.getAttribLocation(shaderProgramTextureGrayScale, 'aVertexColor'),
    },
    uniformLocations: {
      projectionMatrix: gl.getUniformLocation(shaderProgramTextureGrayScale, 'uProjectionMatrix'),
      modelViewMatrix: gl.getUniformLocation(shaderProgramTextureGrayScale, 'uModelViewMatrix'),
    },
  };

  var then = 0;

  // Draw the scene repeatedly
  function render(now) {
    now *= 0.001;  // convert to seconds
    const deltaTime = now - then;
    then = now;
    
    if(hit_timer != 0)
    {
      hit_timer-- ;
    }else{
      if(hit_no !=0)
      {
        hit_no--;
      }
      hit_timer = 300 ; 
    }

    if(initial_timer >= 0)
      initial_timer-- ;

    if(timer_jumpingboots >= 0)
      timer_jumpingboots-- ; 
    
    if(timer_flyingboost >= 0)
      timer_flyingboost-- ; 

    if(timer_invulnerability >= 0)
      timer_invulnerability-- ; 

    if(timer_duck >= 0)
      timer_duck-- ; 

    if(grayscale == false)
      drawScene(gl, programInfo, programInfoTexture, deltaTime, groundtexture, treetexture, graveltexture, trainfront, walltexture, electricboxtexture, personbacktexture, policebacktexture, dogtexture);
    else
      drawScene(gl, programInfoGrey, programInfoTextureGreyScale, deltaTime, groundtexture, treetexture, graveltexture, trainfront, walltexture, electricboxtexture, personbacktexture, policebacktexture, dogtexture);

    requestAnimationFrame(render);
    
    // delay = 100000 ; 
    // console.log("Wait called") ; 
    // function sleep(ms) {
    //   return new Promise(resolve => setTimeout(resolve, ms));
    // }
    
    // async function demo() {
    //   console.log('Taking a break...');
    //   await sleep(delay);
    //   console.log('Two seconds later');
    // }
    
    // demo();
    sleep(2) ; 

  }
  requestAnimationFrame(render);

}

function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}

//
// Draw the scene.
//
function drawScene(gl, programInfo, programInfoTexture, deltaTime, groundtexture, treetexture, graveltexture, trainfront, walltexture, electricboxtexture, personbacktexture, policebacktexture, dogtexture) {
  // gl.clearColor(135/255,206/255,250/255,1.0);  // Clear to black, fully opaque
  gl.clearColor(0.0,0.0,0.0,1.0);  // Clear to black, fully opaque
  gl.clearDepth(1.0);                 // Clear everything
  gl.enable(gl.DEPTH_TEST);           // Enable depth testing
  gl.depthFunc(gl.LEQUAL);            // Near things obscure far things

  // Clear the canvas before we start drawing on it.

  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

  // Create a perspective matrix, a special matrix that is
  // used to simulate the distortion of perspective in a camera.
  // Our field of view is 45 degrees, with a width/height
  // ratio that matches the display size of the canvas
  // and we only want to see objects between 0.1 units
  // and 100 units away from the camera.

  const fieldOfView = 45 * Math.PI / 180;   // in radians
  const aspect = gl.canvas.clientWidth / gl.canvas.clientHeight;
  const zNear = 0.1;
  const zFar = 100.0;
  const projectionMatrix = mat4.create();

  // note: glmatrix.js always has the first argument
  // as the destination to receive the result.
  mat4.perspective(projectionMatrix,
                   fieldOfView,
                   aspect,
                   zNear,
                   zFar);

  // Set the drawing position to the "identity" point, which is
  // the center of the scene.
    var cameraMatrix = mat4.create();
    mat4.translate(cameraMatrix, cameraMatrix, [0.0, c.pos[1]+4, c.pos[2]+10]);
    var cameraPosition = [
      cameraMatrix[12],
      cameraMatrix[13],
      cameraMatrix[14],
    ];

    var up = [0, 1, 0];
    var placeToLook = [0.0,c.pos[1], c.pos[2]] ; 

    mat4.lookAt(cameraMatrix, cameraPosition, placeToLook, up);

    var viewMatrix = cameraMatrix;//mat4.create();

    // mat4.invert(viewMatrix, cameraMatrix);

    var viewProjectionMatrix = mat4.create();

    mat4.multiply(viewProjectionMatrix, projectionMatrix, viewMatrix);

  c.drawCube(gl, viewProjectionMatrix, programInfo, deltaTime);
  cback.drawBack(gl, viewProjectionMatrix, programInfoTexture, deltaTime, personbacktexture, c.pos, c.rotation) ; 
  c_bb = c.getPlayerBoundingBox() ; 


  for(var i = 0 ; i < f.length ; ++i)
  { 
    if((c.pos[2]-f[i].pos[2]) <= 20.0) 
    {
      f[i].drawFloor(gl, viewProjectionMatrix, programInfoTexture, deltaTime, graveltexture);
    }
    if(c.pos[2] + 8.0 <= f[i].pos[2])
    {
      f.splice(i,1) ; 
    }
  }



  for(var i = 0 ; i < walls.length ; ++i)
  { 
    if((c.pos[2]-walls[i].pos[2]) <= 20.0) 
    {
      walls[i].drawWall(gl, viewProjectionMatrix, programInfoTexture, deltaTime, walltexture);
    }
    if(c.pos[2] + 10.0 <= walls[i].pos[2])
    {
      walls.splice(i,1) ; 
    }
  }


  for(var i = 0 ; i < rail_track.length ; ++i)
  { 
    if((c.pos[2]-rail_track[i].pos[2]) <= 20.0) 
    {
      rail_track[i].drawFloor(gl, viewProjectionMatrix, programInfoTexture, deltaTime, groundtexture);
    }
    if(c.pos[2] + 8.0 <= rail_track[i].pos[2])
    {
      rail_track.splice(i,1) ; 
    }
  }

  for(var i =0 ; i < obstacle_1.length ; ++i)
  { 
    if((c.pos[2] - obstacle_1[i].pos[2]) <= 20.0)
    {
      if(detect_collision(c_bb, obstacle_1[i].getObstacleOneBoundingBox()) && timer_invulnerability <= 0){
        // obstacle_1.splice(i,1) ;
        console.log("Game over") ; 
      }else{
        obstacle_1[i].drawObstacleOne(gl, viewProjectionMatrix, programInfo, deltaTime) ; 
        obstacle_1_face[i].drawFace(gl, viewProjectionMatrix, programInfoTexture, deltaTime, trainfront) ; 
      }
    }
    if(c.pos[2] + 8.0 <= obstacle_1[i].pos[2])
    {
      obstacle_1.splice(i,1) ; 
      obstacle_1_face.splice(i,1) ; 
    }
  }

  for(var i =0 ; i < obstacle_2.length ; ++i)
  { 
    if((c.pos[2] - obstacle_2[i].pos[2]) <= 20.0)
    {
      if(detect_collision(c_bb, obstacle_2[i].getObstacleTwoBoundingBox()) && timer_invulnerability <= 0){
        obstacle_2.splice(i,1) ;
        hit_no++ ; 
        console.log("Hit once") ;
        // continue ;  
      }else{
        obstacle_2[i].drawObstacleTwo(gl, viewProjectionMatrix, programInfoTexture, deltaTime, treetexture) ; 
      }
    }
    if(c.pos[2] + 8.0 <= obstacle_2[i].pos[2])
    {
      obstacle_2.splice(i,1) ; 
    }
  }
  for(var i =0 ; i < obstacle_3.length ; ++i)
  { 
    if((c.pos[2] - obstacle_3[i].pos[2]) <= 20.0)
    {
      if(detect_collision(c_bb, obstacle_3[i].getObstacleThreeBoundingBox()) && timer_invulnerability <= 0 && timer_duck <= 0){
        obstacle_3.splice(i,1) ;
        hit_no++ ; 
        console.log("Hit once") ;
        // continue ;  
      }else{
        obstacle_3[i].drawObstacleThree(gl, viewProjectionMatrix, programInfo, deltaTime) ; 
      }
    }
    if(c.pos[2] + 8.0 <= obstacle_3[i].pos[2])
    {
      obstacle_3.splice(i,1) ; 
    }
  }

  for(var i =0 ; i < obstacle_4.length ; ++i)
  { 
    if((c.pos[2] - obstacle_4[i].pos[2]) <= 20.0)
    {
      if(detect_collision(c_bb, obstacle_4[i].getObstacleFourBoundingBox()) && timer_invulnerability <= 0){
        // obstacle_4.splice(i,1) ;
        console.log("Game over") ; 
      }else{
        obstacle_4[i].drawObstacleFour(gl, viewProjectionMatrix, programInfo, deltaTime) ; 
        obstacle_4_face[i].drawFace(gl, viewProjectionMatrix, programInfoTexture, deltaTime, electricboxtexture) ; 
      }
    }
    if(c.pos[2] + 8.0 <= obstacle_4[i].pos[2])
    {
      obstacle_4.splice(i,1) ; 
      obstacle_4_face.splice(i,1) ; 
    }
  }

  for(var i =0 ; i < powerup_1.length ; ++i)
  { 
    if((c.pos[2] - powerup_1[i].pos[2]) <= 20.0)
    {
      if(detect_collision(c_bb, powerup_1[i].getBoundingBox())){
        powerup_1.splice(i,1) ;
        console.log("JumpingBoots collected") ; 
        timer_jumpingboots = 400 ; 
        continue ; 
      }else{
        powerup_1[i].drawJumpingBoots(gl, viewProjectionMatrix, programInfo, deltaTime) ; 
      }
    }
    if(c.pos[2] + 8.0 <= powerup_1[i].pos[2])
    {
      powerup_1.splice(i,1) ; 
    }
  }

  for(var i =0 ; i < powerup_2.length ; ++i)
  { 
    if((c.pos[2] - powerup_2[i].pos[2]) <= 20.0)
    {
      if(detect_collision(c_bb, powerup_2[i].getBoundingBox())){
        powerup_2.splice(i,1) ;
        console.log("FlyingBoost collected") ; 
        timer_flyingboost = 300 ; 
        c.fly(timer_flyingboost) ; 
        continue ; 
      }else{
        powerup_2[i].drawFlyingBoost(gl, viewProjectionMatrix, programInfo, deltaTime) ; 
      }
    }
    if(c.pos[2] + 8.0 <= powerup_2[i].pos[2])
    {
      powerup_2.splice(i,1) ; 
    }
  }

  for(var i =0 ; i < powerup_3.length ; ++i)
  { 
    if((c.pos[2] - powerup_3[i].pos[2]) <= 20.0)
    {
      if(detect_collision(c_bb, powerup_3[i].getBoundingBox())){
        powerup_3.splice(i,1) ;
        console.log("invulnerability collected") ; 
        console.log(powerup_3[i].pos, i) ; 
        timer_invulnerability = 400 ; 
        continue ; 
      }else{
        powerup_3[i].drawInvulnerability(gl, viewProjectionMatrix, programInfo, deltaTime) ; 
      }
    }
      if(c.pos[2] + 8.0 <= powerup_3[i].pos[2])
      {
        powerup_3.splice(i,1) ; 
      }
  }

  for(var i =0 ; i < coin_list.length ; ++i)
  { 
    if((c.pos[2] - coin_list[i].pos[2]) <= 20.0)
    {
      if(detect_collision(c_bb, coin_list[i].getCoinBoundingBox())){
        coin_list.splice(i,1) ;
        score += 10 ; 
        continue ; 
      }else{
        coin_list[i].drawCoin(gl, viewProjectionMatrix, programInfo, deltaTime);
      }
    }
      if(c.pos[2] + 8.0 <= coin_list[i].pos[2])
      {
        coin_list.splice(i,1) ; 
      }
  }


  p.drawPolice(gl, viewProjectionMatrix, programInfo, deltaTime, hit_no, c.pos[2]) ;
  pback.drawBack(gl, viewProjectionMatrix, programInfoTexture, deltaTime, policebacktexture, p.pos, p.rotation) ; 
  if (c.jumpLevel == 3) 
    doggo.drawDog(gl, viewProjectionMatrix, programInfoTexture, deltaTime, dogtexture, (c.pos[0], c.pos[1], 0.0)) ; 
  else 
    doggo.drawDog(gl, viewProjectionMatrix, programInfoTexture, deltaTime, dogtexture, c.pos) ; 


  // tf.drawFloor(gl, viewProjectionMatrix, programInfoTexture, deltaTime, groundtexture) ; 
  
  if(initial_timer <= 0 && hit_no >= 2)
  {
    console.log("Game over, caught by police") ; 
  }

}

//
// Initialize a shader program, so WebGL knows how to draw our data
//
function initShaderProgram(gl, vsSource, fsSource) {
  const vertexShader = loadShader(gl, gl.VERTEX_SHADER, vsSource);
  const fragmentShader = loadShader(gl, gl.FRAGMENT_SHADER, fsSource);

  // Create the shader program

  const shaderProgram = gl.createProgram();
  gl.attachShader(shaderProgram, vertexShader);
  gl.attachShader(shaderProgram, fragmentShader);
  gl.linkProgram(shaderProgram);

  // If creating the shader program failed, alert

  if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
    alert('Unable to initialize the shader program: ' + gl.getProgramInfoLog(shaderProgram));
    return null;
  }

  return shaderProgram;
}

//
// creates a shader of the given type, uploads the source and
// compiles it.
//
function loadShader(gl, type, source) {
  const shader = gl.createShader(type);

  // Send the source to the shader object

  gl.shaderSource(shader, source);

  // Compile the shader program

  gl.compileShader(shader);

  // See if it compiled successfully

  if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
    alert('An error occurred compiling the shaders: ' + gl.getShaderInfoLog(shader));
    gl.deleteShader(shader);
    return null;
  }

  return shader;
}

//
// Initialize a texture and load an image.
// When the image finished loading copy it into the texture.
//
function loadTexture(gl, url) {
  const texture = gl.createTexture();
  gl.bindTexture(gl.TEXTURE_2D, texture);

  // Because images have to be download over the internet
  // they might take a moment until they are ready.
  // Until then put a single pixel in the texture so we can
  // use it immediately. When the image has finished downloading
  // we'll update the texture with the contents of the image.
  const level = 0;
  const internalFormat = gl.RGBA;
  const width = 1;
  const height = 1;
  const border = 0;
  const srcFormat = gl.RGBA;
  const srcType = gl.UNSIGNED_BYTE;
  const pixel = new Uint8Array([0, 0, 255, 255]);  // opaque blue
  gl.texImage2D(gl.TEXTURE_2D, level, internalFormat,
                width, height, border, srcFormat, srcType,
                pixel);

  const image = new Image();
  image.onload = function() {
    gl.bindTexture(gl.TEXTURE_2D, texture);
    gl.texImage2D(gl.TEXTURE_2D, level, internalFormat,
                  srcFormat, srcType, image);

    // WebGL1 has different requirements for power of 2 images
    // vs non power of 2 images so check if the image is a
    // power of 2 in both dimensions.
    if (isPowerOf2(image.width) && isPowerOf2(image.height)) {
       // Yes, it's a power of 2. Generate mips.
       gl.generateMipmap(gl.TEXTURE_2D);
    } else {
       // No, it's not a power of 2. Turn of mips and set
       // wrapping to clamp to edge
       gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
       gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
       gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
    }
  };
  image.src = url;

  return texture;
}

function isPowerOf2(value) {
  return (value & (value - 1)) == 0;
}



document.addEventListener('keydown', function(event) {
  if (event.keyCode == 37) {
    console.log("Move block left") ; 
    c.left() ; 
    p.left() ; 
    
  }
  else if (event.keyCode == 39) {
    console.log("Move block right") ; 
    c.right() ; 
    p.right() ; 
  }
  else if (event.keyCode == 32){
    console.log("Jump") ; 
    if(timer_jumpingboots <= 0)
    {
      c.jump(1,0) ;
      p.jump(1,0) ; 
    }else{
      c.jump(2,0) ;
      p.jump(2,0) ; 
    }
  }
  else if(event.keyCode == 40){
    timer_duck = 60 ; 
    c.duck(60) ; 
  }
  else if(event.keyCode == 66){
    grayscale = !grayscale ; 
  }

}, true);



function detect_collision(a,b) {
  return (Math.abs(a['x'] - b['x']) * 2 < (a['width'] + b['width'])) &&
         (Math.abs(a['y'] - b['y']) * 2 < (a['height'] + b['height'])) &&
         (Math.abs(a['z'] - b['z']) * 2 < (a['depth'] + b['depth']));
}
