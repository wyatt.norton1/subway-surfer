/// <reference path="webgl.d.ts" />

let coin = class {
    constructor(gl, pos) {
        this.positionBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.positionBuffer);

        this.track = 0 ;
        this.jumpLevel = 0;
        this.rising = false ;
        this.positions = [] ; 

        this.c1_nos = 20 ; 
        var c1_r = 0.5 ; 

        for(var i=0 ; i< this.c1_nos ; ++i)
        {   
            this.positions.push(c1_r * Math.cos(2*Math.PI*i/this.c1_nos)) ; 
            this.positions.push( c1_r * Math.sin(2*Math.PI*i/this.c1_nos)) ; 
            this.positions.push(0.0) ; 
            this.positions.push(c1_r * Math.cos(2*Math.PI*[(i+1)%this.c1_nos]/this.c1_nos)) ; 
            this.positions.push(c1_r * Math.sin(2*Math.PI*[(i+1)%this.c1_nos]/this.c1_nos)) ; 
            this.positions.push(0.0) ; 
            this.positions.push(0.0) ; 
            this.positions.push(0.0) ; 
            this.positions.push(0.0) ; 
            // vertex_buffer_data_lava[9*i] = c1_r * Math.cos(2*M_PI*i/c1_nos) ; 
            // vertex_buffer_data_lava[9*i+1] = c1_r * Math.sin(2*M_PI*i/c1_nos) ; 
            // vertex_buffer_data_lava[9*i+2] = 0.0
            // vertex_buffer_data_lava[9*i+3] =  c1_r * Math.cos(2*M_PI*[(i+1)%c1_nos]/c1_nos); 
            // vertex_buffer_data_lava[9*i+4] =  c1_r * Math.sin(2*M_PI*[(i+1)%c1_nos]/c1_nos) ; 
            // vertex_buffer_data_lava[9*i+5] =  0.0 ;
            // vertex_buffer_data_lava[9*i+6] =  0.0; 
            // vertex_buffer_data_lava[9*i+7] =  0.0; 
            // vertex_buffer_data_lava[9*i+8] =  0.0; 
        }
    

        this.rotation = 0;

        this.pos = pos;

        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this.positions), gl.STATIC_DRAW);

        var colors = [];

        for (var j = 0; j < this.c1_nos; ++j) {
            const c = [255.0/255,215.0/255,0.0,1.0] ;   

            // Repeat each color four times for the four vertices of the face
            colors = colors.concat(c, c, c);
        }

        const colorBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(colors), gl.STATIC_DRAW);

        // Build the element array buffer; this specifies the indices
        // into the vertex arrays for each face's vertices.

        const indexBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);

        // This array defines each face as two triangles, using the
        // indices into the vertex array to specify each triangle's
        // position.

        // const indices = [
        //     0, 1, 2,    0, 2, 3, // front
        //     4, 5, 6,    4, 6, 7,
        //     8, 9, 10,   8, 10, 11,
        //     12, 13, 14, 12, 14, 15,
        //     16, 17, 18, 16, 18, 19,
        //     20, 21, 22, 20, 22, 23, 
        // ];

        var indices = [] ;

        for(var i = 0 ; i < 3 * this.c1_nos ; ++i)
        {
            indices.push(3*i) ; 
            indices.push(3*i+1) ; 
            indices.push(3*i+2) ; 
        }

        // Now send the element array to GL

        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER,
            new Uint16Array(indices), gl.STATIC_DRAW);

        this.buffer = {
            position: this.positionBuffer,
            color: colorBuffer,
            indices: indexBuffer,
        }

    }

    drawCoin(gl, projectionMatrix, programInfo, deltaTime) {


        const modelViewMatrix = mat4.create();
        mat4.translate(
            modelViewMatrix,
            modelViewMatrix,
            this.pos
        );
        
        this.rotation += Math.PI / (((Math.random()) % 100) + 50);
        // this.rotation += deltaTime

        mat4.rotate(modelViewMatrix,
            modelViewMatrix,
            this.rotation,
            [0, 1, 0]);

        {
            const numComponents = 3;
            const type = gl.FLOAT;
            const normalize = false;
            const stride = 0;
            const offset = 0;
            gl.bindBuffer(gl.ARRAY_BUFFER, this.buffer.position);
            gl.vertexAttribPointer(
                programInfo.attribLocations.vertexPosition,
                numComponents,
                type,
                normalize,
                stride,
                offset);
            gl.enableVertexAttribArray(
                programInfo.attribLocations.vertexPosition);
        }

        // Tell WebGL how to pull out the colors from the color buffer
        // into the vertexColor attribute.
        {
            const numComponents = 4;
            const type = gl.FLOAT;
            const normalize = false;
            const stride = 0;
            const offset = 0;
            gl.bindBuffer(gl.ARRAY_BUFFER, this.buffer.color);
            gl.vertexAttribPointer(
                programInfo.attribLocations.vertexColor,
                numComponents,
                type,
                normalize,
                stride,
                offset);
            gl.enableVertexAttribArray(
                programInfo.attribLocations.vertexColor);
        }

        // Tell WebGL which indices to use to index the vertices
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.buffer.indices);

        // Tell WebGL to use our program when drawing

        gl.useProgram(programInfo.program);

        // Set the shader uniforms

        gl.uniformMatrix4fv(
            programInfo.uniformLocations.projectionMatrix,
            false,
            projectionMatrix);
        gl.uniformMatrix4fv(
            programInfo.uniformLocations.modelViewMatrix,
            false,
            modelViewMatrix);

        {
            const vertexCount = 3* this.c1_nos ; 
            const type = gl.UNSIGNED_SHORT;
            const offset = 0;
            gl.drawElements(gl.TRIANGLES, vertexCount, type, offset);
        }

    }

    getCoinBoundingBox(){
        var a = {
            height : 2.0,
            width : 2.0,
            depth : 2.0, 
            x : this.pos[0],
            y  : this.pos[1],
            z : this.pos[2] } ;

        return a ; 
    }

};